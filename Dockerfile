FROM openjdk:8

MAINTAINER PiLights Team <contact@pilights.de>

RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections

RUN apt-get update && apt-get upgrade -y
RUN apt-get install software-properties-common git apt-utils -y
#RUN \
#  echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | debconf-set-selections && \
#  add-apt-repository -y ppa:webupd8team/java && \
#  apt-get update && \
#  apt-get install -y oracle-java8-installer && \
#  rm -rf /var/lib/apt/lists/* && \
#  rm -rf /var/cache/oracle-jdk8-installer
#RUN apt-get install oracle-java8-set-default
#RUN add-apt-repository ppa:openjdk-r/ppa
#RUN apt-get update
#RUN apt-get install openjdk-8-jdk -y
RUN java -version
#RUN export JAVA_HOME=/usr/lib/jvm/java-8-oracle/bin

#RUN apt-get install --yes openjdk-8-jre icedtea-8-plugin  openjdk-8-doc openjdk-8-jre-headless openjdk-8-source 

#ENV JAVA_HOME /usr/lib/jvm/java-8-oracle

RUN apt-get --quiet update --yes
RUN apt-get --quiet install --yes wget tar unzip lib32stdc++6 lib32z1 git
RUN wget --quiet --output-document=android-sdk.tgz https://dl.google.com/android/repository/tools_r25.2.3-linux.zip
RUN unzip -q android-sdk.tgz -d android-sdk-linux

ENV ANDROID_HOME /android-sdk-linux

RUN echo y | android-sdk-linux/tools/android --silent update sdk --no-ui --all --filter android-28
RUN echo y | android-sdk-linux/tools/android --silent update sdk --no-ui --all --filter platform-tools
RUN echo y | android-sdk-linux/tools/android --silent update sdk --no-ui --all --filter build-tools-28.0.2
RUN echo y | android-sdk-linux/tools/android --silent update sdk --no-ui --all --filter extra-android-m2repository
RUN echo y | android-sdk-linux/tools/android --silent update sdk --no-ui --all --filter extra-google-m2repository


